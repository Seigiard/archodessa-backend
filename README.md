# archodessa-sails

## HOW TO RUN

    npm run db
    sails lift

## HOW TO ADD MODEL

    sails generate api <foo>

Generate api/models/Foo.js and api/controllers/FooController.js

http://sailsjs.org/documentation/reference/command-line-interface/sails-generate
